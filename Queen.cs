using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IS_1
{
    public class Queen
    {
        // позиция на доске по горизонтали
        private int x;
        // позиция на доске по вертикали
        private int y;
        //
        private Form1 form;
        public Queen(int x, int y, Form1 mainWindow)
        {
            set_form(mainWindow);
            set_x(x);
            set_y(y);
        }

        public int get_x()
        {
            return this.x;
        }
        public int get_y()
        {
            return this.y;
        }
        public Form1 get_form()
        {
            return this.form;
        }

        public bool set_x(int x)
        {
            Form1 childForm = get_form();
            TextBox textBox_width = childForm.get_textBox_width();
            int width = Convert.ToInt32(textBox_width.Text);

            if (x < width)
            {
                this.x = x;
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool set_y(int y)
        {

            Form1 childForm = get_form();
            TextBox textBox_height = childForm.get_textBox_height();
            int height = Convert.ToInt32(textBox_height.Text);

            if (y < height)
            {
                this.y = y;
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool set_form(Form1 form)
        {
            this.form = form;
            return true;
        }
        public bool is_attacked(int other_x, int other_y)
        {
            //бьёт, если разницы между координатами x и между координатами y равны.
            if (Math.Abs(this.get_x() - other_x) == Math.Abs(this.get_y() - other_y))
            {
                return true;
            }
            //бьёт, если стоят рядом
            if (((this.get_x() - other_x) == 0) || ((this.get_y() - other_y) == 0))
            {
                return true;
            }
            //бьёт, если координаты по горизонатли или вертикали равны.
            if ((this.get_x() == other_x) || (this.get_y() == other_y))
            {
                return true;
            }
            return false;
        }
    }
}
