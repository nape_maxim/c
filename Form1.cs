using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IS_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button_generate_chessboard_Click(object sender, EventArgs e)
        {
            int columnsNum = Convert.ToInt32(textBox_width.Text);
            int rowsNum = Convert.ToInt32(textBox_height.Text);

            foreach (DataGridViewRow row in chessboard.Rows)
            {
                foreach (DataGridViewCell cell in row.Cells)
                {
                    cell.Style.BackColor = Color.White;
                }
            }
            chessboard.ColumnCount = columnsNum;
            foreach (DataGridViewColumn column in chessboard.Columns)
            {
                column.Width = ((chessboard.Size.Width - 5) / columnsNum);
                column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            }
            chessboard.RowCount = rowsNum;
            foreach (DataGridViewRow row in chessboard.Rows)
            {
                row.Height = ((chessboard.Size.Height - 5) / rowsNum);
            }

            if ((textBox_coordinate_X.Text != "") && (textBox_coordinate_Y.Text != ""))
            {
                button_calculate.Enabled = true;
            }
        }

        public TextBox get_textBox_width()
        {
            return textBox_width;
        }
        public TextBox get_textBox_height()
        {
            return textBox_height;
        }

        private void button_calculate_Click(object sender, EventArgs e)
        {
            var queens = new List<Queen>();
            if ((Convert.ToInt32(textBox_coordinate_X.Text) > chessboard.Columns.Count) || (Convert.ToInt32(textBox_coordinate_Y.Text) > chessboard.Rows.Count))
            {
                MessageBox.Show("Позиция первого ферзя должна быть в пределах доски.");
                textBox_coordinate_X.Text = "";
                textBox_coordinate_Y.Text = "";
            }
            else
            {
                int start_x = Convert.ToInt32(textBox_coordinate_X.Text);
                int start_y = Convert.ToInt32(textBox_coordinate_Y.Text);

                foreach (DataGridViewRow row in chessboard.Rows)
                {
                    foreach (DataGridViewCell cell in row.Cells)
                    {
                        cell.Style.BackColor = Color.White;
                    }
                }

                Queen first_queen = new Queen(start_x, start_y, this);
                queens.Add(first_queen);
                chessboard.Rows[queens[queens.Count - 1].get_y()].Cells[queens[queens.Count - 1].get_x()].Style.BackColor = Color.Black;

                if (placement_queens(0, 0, queens) == true)
                {
                    label1.Text = "ВЫПОЛНЕНО!";
                }
                else
                {
                    chessboard.Rows[queens[queens.Count - 1].get_y()].Cells[queens[queens.Count - 1].get_x()].Style.BackColor = Color.White;
                    label1.Text = "НЕ МОГУ РАЗМЕСТИТЬ ФЕРЗЕЙ.";
                }
            }
        }

        public bool placement_queens(int cur_x, int cur_y, List<Queen> queens)
        {
            int columnsNum = Convert.ToInt32(textBox_width.Text);
            int rowsNum = Convert.ToInt32(textBox_height.Text);
            int[] new_position = new int[2];
            bool is_placed = false;
            while (queens.Count != 8)
            {
                new_position = calculate_position(cur_x, cur_y, queens);
                while (new_position[0] == -1)
                {
                    cur_x += 1;
                    cur_x = correct_x(cur_x);
                    cur_y = correct_y(cur_y, cur_x);
                    if (cur_y == rowsNum)
                    {
                        return false;
                    }
                    new_position = calculate_position(cur_x, cur_y, queens);
                }
                queens.Add(new Queen(new_position[0], new_position[1], this));
                chessboard.Rows[queens[queens.Count - 1].get_y()].Cells[queens[queens.Count - 1].get_x()].Style.BackColor = Color.Black;
                is_placed = placement_queens(cur_x, cur_y, queens);
                if (is_placed == false)
                {
                    chessboard.Rows[queens[queens.Count - 1].get_y()].Cells[queens[queens.Count - 1].get_x()].Style.BackColor = Color.White;
                    queens.RemoveAt(queens.Count - 1);
                    cur_x += 1;
                    if (cur_x == columnsNum)
                    {
                        cur_x = correct_x(cur_x);
                        cur_y = correct_y(cur_y, cur_x);
                        return false;
                    }
                }
            }
            return true;
        }
        public int[] calculate_position(int cur_x, int cur_y, List<Queen> queens)
        {
            int[] result = new int[2] { -1, -1 };
            bool is_possible_position = true;

            int columnsNum = Convert.ToInt32(textBox_width.Text);
            int rowsNum = Convert.ToInt32(textBox_height.Text);
            foreach (Queen queen in queens)
            {
                if ((cur_x) == columnsNum)
                {
                    cur_x = 0;
                    cur_y += 1;
                }
                if (cur_y != rowsNum)
                {
                    if (queen.is_attacked(cur_x, cur_y))
                    {
                        is_possible_position = false;
                        break;
                    }
                }
            }
            if (is_possible_position == true)
            {
                result[0] = cur_x;
                result[1] = cur_y;
                return result;
            }
            else
            {
                return result;
            }
        }

        public int correct_x(int cur_x)
        {
            int columnsNum = Convert.ToInt32(textBox_width.Text);
            if (cur_x == columnsNum)
            {
                return 0;
            }
            else
            {
                return cur_x;
            }
        }
        public int correct_y(int cur_y, int cur_x)
        {
            if (cur_x == 0)
            {
                return cur_y + 1;
            }
            else
            {
                return cur_y;
            }
        }

        private void chessboard_SelectionChanged(object sender, EventArgs e)
        {
            this.chessboard.ClearSelection();
        }


        private bool is_allow_symbol(string keyChar)
        {
            string[] symbols = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "\b" };
            foreach (string symbol in symbols)
            {
                if (keyChar == symbol)
                {
                    return true;
                }
            }
            return false;
        }
        private void textBox_width_TextChanged(object sender, EventArgs e)
        {
            if ((textBox_width.Text == "") || (textBox_height.Text == ""))
            {
                button_generate_chessboard.Enabled = false;
            }
            else
            {
                button_generate_chessboard.Enabled = true;
            }
        }

        private void textBox_height_TextChanged(object sender, EventArgs e)
        {
            if ((textBox_width.Text == "") || (textBox_height.Text == ""))
            {
                button_generate_chessboard.Enabled = false;
            }
            else
            {
                button_generate_chessboard.Enabled = true;
            }
        }

        private void textBox_width_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!is_allow_symbol(e.KeyChar.ToString()))
            {
                e.Handled = true;
            }
        }

        private void textBox_height_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!is_allow_symbol(e.KeyChar.ToString()))
            {
                e.Handled = true;
            }
        }

        private void textBox_coordinate_X_TextChanged(object sender, EventArgs e)
        {
            if ((textBox_coordinate_X.Text == "") || (textBox_coordinate_Y.Text == ""))
            {
                button_calculate.Enabled = false;
            }
            else
            {
                button_calculate.Enabled = true;
            }
        }

        private void textBox_coordinate_Y_TextChanged(object sender, EventArgs e)
        {
            if ((textBox_coordinate_X.Text == "") || (textBox_coordinate_Y.Text == ""))
            {
                button_calculate.Enabled = false;
            }
            else
            {
                button_calculate.Enabled = true;
            }
        }

        private void textBox_coordinate_X_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!is_allow_symbol(e.KeyChar.ToString()))
            {
                e.Handled = true;
            }
        }

        private void textBox_coordinate_Y_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!is_allow_symbol(e.KeyChar.ToString()))
            {
                e.Handled = true;
            }
        }

        private void button_generate_chessboard_EnabledChanged(object sender, EventArgs e)
        {
            
        }

        private void button_calculate_EnabledChanged(object sender, EventArgs e)
        {
            if ((chessboard.Rows.Count != 0) && (chessboard.Columns.Count != 0))
            {
                button_calculate.Enabled = true;
            }
            else
            {
                button_calculate.Enabled = false;
            }
        }
    }
}
